package CSParse;

import java.util.LinkedList;
import java.util.Vector;

import command.Commander;
import command.SameList;
import search.Logicks;
import search_result.SentenceResult;
import syntree.SynTree;

public class AndNode extends BinaryLogicNode {
	SameList list_o_same;

	public AndNode(LinkedList qnodes) {
		super.Init(qnodes);
	}

	// this.PrintToSystemErr();}

	public SentenceResult evaluate(SynTree sparse) {
		SentenceResult sr1, sr2, sr3, sr4;
		Vector arg_List;
		int qdex;
		QueryNode qn3;

		sr1 = (this.qn1).evaluate(sparse);
		if ((this.qnodes).size() == 1) {
			return sr1;
		}
		sr2 = (this.qn2).evaluate(sparse);
		args_for_same = new Vector();
		this.addToArgsList(this.qn1, args_for_same);
		this.addToArgsList(this.qn2, args_for_same);
		list_o_same = Commander.Sameness(args_for_same);
		sr3 = (Logicks.And(sr1, sr2, list_o_same));
		for (qdex = 2; qdex < (this.qnodes).size(); qdex++) {
			qn3 = (QueryNode) qnodes.get(qdex);
			addToArgsList(qn3, args_for_same);
			list_o_same = Commander.Sameness(args_for_same);
			sr4 = qn3.evaluate(sparse);
			sr3 = (Logicks.And(sr3, sr4, list_o_same));
		}
		return sr3;
	}

	private void addToArgsList(QueryNode qn, Vector arg_list) {

		for (int i = 0; i < (qn.args_for_same).size(); i++) {
			arg_list.addElement((String) (qn.args_for_same).elementAt(i));
		}
		return;
	}

	public void PrintToSystemErr() {
		System.err.println("AndNode:");
	}
}

