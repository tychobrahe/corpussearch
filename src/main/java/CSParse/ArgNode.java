package CSParse;

import basicinfo.ArgList;
import command.CommandAux;
import search_result.SentenceResult;
import syntree.SynTree;

public class ArgNode extends QueryNode {
	String args;
	// prefix_args is used for the same_list. It's args with prefix,
	// if one exists.
	String prefix_args, prefix, curlies;
	ArgList args_for_search;
	boolean denied = false;

	public ArgNode(String argo, boolean is_denied, String prefox, String curlies) {
		if (is_denied) {
			this.prefix_args = prefox + "!" + argo;
		} else {
			this.prefix_args = prefox + argo;
		}
		this.args = argo;
		this.curlies = curlies;
		this.denied = is_denied;
		this.prefix = prefox;
		this.args_for_search = new ArgList(this.args);
		CommandAux.CheckListForIgnore(this.args_for_search);
		// this.PrintToSystemErr();
	}

	public boolean IsDenied() {
		return denied;
	}

	// "evaluate" is here so that ArgNode can inherit
	// from QueryNode. It is never called.
	public SentenceResult evaluate(SynTree sparse) {
		SentenceResult sr = new SentenceResult();
		return sr;
	}

	public void PrintToSystemErr() {
		System.err.println("ArgNode:  ");
		System.err.println("curlies:  " + this.curlies);
		System.err.println("args_for_search: " + args_for_search);
		System.err.println("");
	}
}

