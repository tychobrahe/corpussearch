package CSParse;

import java.util.LinkedList;
import java.util.Vector;

public abstract class BinaryLogicNode extends QueryNode { // AND, OR, XOR.

	protected BinaryLogicNode() {
	}

	protected BinaryLogicNode(LinkedList qnodes) {
		Init(qnodes);
	}

	protected void Init(LinkedList qnodes) {
		QueryNode qn;
		int i, j;
		Vector qn_same;
		Vector qn_curl;

		this.qnodes = qnodes;
		if (qnodes.size() > 0) {
			this.qn1 = (QueryNode) qnodes.get(0);
		}
		if (qnodes.size() > 1) {
			this.qn2 = (QueryNode) qnodes.get(1);
		}
		this.args_for_same = new Vector();
		this.curlies_list = new Vector();
		for (i = 0; i < qnodes.size(); i++) {
			qn = (QueryNode) qnodes.get(i);
			qn_same = qn.args_for_same;
			qn_curl = qn.curlies_list;
			for (j = 0; j < qn_same.size(); j++) {
				(this.args_for_same).addElement((String) qn_same.elementAt(j));
			}
			for (j = 0; j < qn_curl.size(); j++) {
				(this.curlies_list).addElement((String) qn_curl.elementAt(j));
			}
		}
	}

	public void PrintToSystemErr() {
		System.err.println("BinaryLogicNode:  ");
	}

	protected QueryNode qn1;
	protected QueryNode qn2;
	protected LinkedList qnodes;
}

