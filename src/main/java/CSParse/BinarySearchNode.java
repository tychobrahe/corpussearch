package CSParse;

import java.util.Vector;

import basicinfo.ArgList;

public abstract class BinarySearchNode extends QueryNode {
	ArgNode arg1, arg2;
	ArgList x_List, y_List;

	protected BinarySearchNode() {
	}

	protected BinarySearchNode(ArgNode arg1, ArgNode arg2) {
		Init(arg1, arg2);
	}

	protected void Init(ArgNode arg1, ArgNode arg2) {
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.x_List = arg1.args_for_search;
		this.y_List = arg2.args_for_search;
		this.args_for_same = new Vector();
		args_for_same.addElement(arg1.prefix_args);
		args_for_same.addElement(arg2.prefix_args);
		this.curlies_list = new Vector();
		curlies_list.addElement(arg1.curlies);
		curlies_list.addElement(arg2.curlies);
		// this.PrintToSystemErr();
	}

	public void PrintToSystemErr() {
		System.err.println("in BinarySearchNode:  ");
		System.err.println("x_List:  " + x_List);
		System.err.println("y_list:  " + y_List);
		System.err.println("args_for_same: " + args_for_same);
	}
}

