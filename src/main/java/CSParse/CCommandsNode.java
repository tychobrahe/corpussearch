package CSParse;

import search.CCommands;
import search_result.SentenceResult;
import syntree.SynTree;

public class CCommandsNode extends BinarySearchNode {

	public CCommandsNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(CCommands.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(CCommands.Not_y(sparse, x_List, y_List));
		}
		return Record(CCommands.Plain(sparse, x_List, y_List));
	}
}

