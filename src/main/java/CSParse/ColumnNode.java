package CSParse;

import search.Column;
import search_result.SentenceResult;
import syntree.SynTree;

public class ColumnNode extends TrinarySearchNode {

	public ColumnNode(ArgNode arg1, IntArgNode arg2, ArgNode arg3) {
		super.Init(arg1, arg2, arg3);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg2.IsDenied()) {
			return Record(Column.Not_y(sparse, x_List, arg_int, y_List));
		}
		return Record(Column.Plain(sparse, x_List, arg_int, y_List));
	}
}
