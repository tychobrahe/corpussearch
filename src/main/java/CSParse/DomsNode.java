package CSParse;

import search.Dominates;
import search_result.SentenceResult;
import syntree.SynTree;


public class DomsNode extends BinarySearchNode {

	public DomsNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(Dominates.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(Dominates.Not_y(sparse, x_List, y_List));
		}
		return Record(Dominates.Plain(sparse, x_List, y_List));
	}
}

