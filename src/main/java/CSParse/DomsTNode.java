package CSParse;

import search.DominatesT;
import search_result.SentenceResult;
import syntree.SynTree;


public class DomsTNode extends BinarySearchNode {

	public DomsTNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(DominatesT.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(DominatesT.Not_y(sparse, x_List, y_List));
		}
		return Record(DominatesT.Plain(sparse, x_List, y_List));
	}
}

