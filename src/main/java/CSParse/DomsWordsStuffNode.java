package CSParse;

import search.DomsWordsAssign;
import search_result.SentenceResult;
import syntree.SynTree;

public class DomsWordsStuffNode extends HackIntBinarySearchNode {

	public DomsWordsStuffNode(ArgNode arg1, IntArgNode intarg1, String which) {
		super.Init(arg1, intarg1, which);
	}

	public SentenceResult evaluate(SynTree sparse) {
		return Record(DomsWordsAssign.Plain(sparse, x_List, arg_int, which_search));
	}
}

