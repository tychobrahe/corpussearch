package CSParse;

import search.Exists;
import search_result.SentenceResult;
import syntree.SynTree;

public class ExistsNode extends UnarySearchNode {

	public ExistsNode(ArgNode arg1) {
		super.Init(arg1);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(Exists.Not_x(sparse, x_List));
		}
		return Record(Exists.Plain(sparse, x_List));
	}
}

