package CSParse;

import java.util.Vector;

import basicinfo.ArgList;

/*
 * the following SHAMELESS HACK was implemented to deal with the too-many-tokens
 * problem. I found that javacc could not handle more tokens normally. To work
 * around this, I collapsed several tokens together, to be differentiated later
 * in the program.
 */
abstract class HackIntBinarySearchNode extends QueryNode {
	// DomsWords, DomsWordsLess...
	ArgNode arg1;
	IntArgNode intarg1;
	ArgList x_List;
	String which_search;
	int arg_int;

	protected HackIntBinarySearchNode() {
	}

	protected HackIntBinarySearchNode(ArgNode arg1, IntArgNode intarg1, String ws) {
		Init(arg1, intarg1, ws);
	}

	protected void Init(ArgNode arg1, IntArgNode intarg1, String ws) {
		this.which_search = ws;
		this.arg1 = arg1;
		this.intarg1 = intarg1;
		this.x_List = arg1.args_for_search;
		this.arg_int = intarg1.int_arg;
		this.args_for_same = new Vector();
		args_for_same.addElement(arg1.prefix_args);
		args_for_same.addElement(arg1.prefix_args);
		this.curlies_list = new Vector();
		curlies_list.addElement(arg1.curlies);
		curlies_list.addElement(arg1.curlies);
		// this.PrintToSystemErr();
	}

	public void PrintToSystemErr() {
		System.err.println("in HackIntBinarySearchNode:  ");
		System.err.println("which_search: " + which_search);
		System.err.println("x_List:  " + x_List);
		System.err.println("args_for_same: " + args_for_same);
		System.err.println("arg_int: " + arg_int);
	}
}

