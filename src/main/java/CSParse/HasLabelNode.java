package CSParse;

import search.hasLabel;
import search_result.SentenceResult;
import syntree.SynTree;


public class HasLabelNode extends BinarySearchNode {

	public HasLabelNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(hasLabel.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(hasLabel.Not_y(sparse, x_List, y_List));
		}
		return Record(hasLabel.Plain(sparse, x_List, y_List));
	}
}

