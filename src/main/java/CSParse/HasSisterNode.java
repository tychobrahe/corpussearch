package CSParse;

import search.HasSister;
import search_result.SentenceResult;
import syntree.SynTree;


public class HasSisterNode extends BinarySearchNode {

	public HasSisterNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(HasSister.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(HasSister.Not_y(sparse, x_List, y_List));
		}
		return Record(HasSister.Plain(sparse, x_List, y_List));
	}
}

