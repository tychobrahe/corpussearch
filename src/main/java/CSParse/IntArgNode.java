package CSParse;

public class IntArgNode {
	int int_arg;
	Integer arg_Int;

	public IntArgNode(String argo) {
		arg_Int = new Integer(argo);
		int_arg = arg_Int.intValue();
		// this.PrintToSystemErr();
	}

	public void PrintToSystemErr() {
		System.err.println("IntArgNode:  ");
		System.err.println(int_arg);
	}
}

