package CSParse;

import java.util.Vector;

import basicinfo.ArgList;

public abstract class IntBinarySearchNode extends QueryNode { // DomsWords,
	// DomsWordsLess...
	ArgNode arg1;
	IntArgNode intarg1;
	ArgList x_List;
	int arg_int;

	protected IntBinarySearchNode() {
	}

	protected IntBinarySearchNode(ArgNode arg1, IntArgNode intarg1) {
		Init(arg1, intarg1);
	}

	protected void Init(ArgNode arg1, IntArgNode intarg1) {
		this.arg1 = arg1;
		this.intarg1 = intarg1;
		this.x_List = arg1.args_for_search;
		this.arg_int = intarg1.int_arg;
		this.args_for_same = new Vector();
		args_for_same.addElement(arg1.prefix_args);
		args_for_same.addElement(arg1.prefix_args);
		this.curlies_list = new Vector();
		curlies_list.addElement(arg1.curlies);
		curlies_list.addElement(arg1.curlies);
		// this.PrintToSystemErr();
	}

	public void PrintToSystemErr() {
		System.err.println("in IntBinarySearchNode:  ");
		System.err.println("x_List:  " + x_List);
		System.err.println("args_for_same: " + args_for_same);
		System.err.println("arg_int: " + arg_int);
	}
}
