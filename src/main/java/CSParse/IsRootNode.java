package CSParse;

import search.isRoot;
import search_result.SentenceResult;
import syntree.SynTree;

public class IsRootNode extends UnarySearchNode {

	public IsRootNode(ArgNode arg1) {
		super.Init(arg1);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(isRoot.Not_x(sparse, x_List));
		}
		return Record(isRoot.Plain(sparse, x_List));
	}
}

