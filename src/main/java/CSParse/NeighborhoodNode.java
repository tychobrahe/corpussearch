package CSParse;

import search.Neighborhood;
import search_result.SentenceResult;
import syntree.SynTree;

public class NeighborhoodNode extends TrinarySearchNode {

	public NeighborhoodNode(ArgNode arg1, IntArgNode arg2, ArgNode arg3) {
		super.Init(arg1, arg2, arg3);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg2.IsDenied()) {
			return Record(Neighborhood.Not_y(sparse, x_List, y_List, arg_int));
		}
		return Record(Neighborhood.Plain(sparse, x_List, y_List, arg_int));
	}
}

