package CSParse;

import search.Logicks;
import search_result.SentenceResult;
import syntree.SynTree;

public class NotNode extends QueryNode {
	protected QueryNode qn;

	protected NotNode(QueryNode qn1) {
		this.qn = qn1;
		this.args_for_same = qn1.args_for_same;
		this.curlies_list = qn1.curlies_list;
		// this.PrintToSystemErr();
	}

	public SentenceResult evaluate(SynTree sparse) {
		SentenceResult sr;

		sr = (this.qn).evaluate(sparse);
		return Record(Logicks.Not(sparse, sr, args_for_same.size(), hist));
	}

	public void PrintToSystemErr() {
		System.err.println("NotNode:");
		System.err.println("args_for_same.size():  " + args_for_same.size());
	}
}

