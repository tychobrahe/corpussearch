package CSParse;

import java.util.LinkedList;

import search.Logicks;
import search_result.SentenceResult;
import syntree.SynTree;

public class OrNode extends BinaryLogicNode {

	public OrNode(LinkedList qnodes) {
		super.Init(qnodes);
	}

	// this.PrintToSystemErr();}

	public SentenceResult evaluate(SynTree sparse) {
		SentenceResult sr1, sr2, sr3, sr4;
		QueryNode qn3;
		int qdex;

		sr1 = (this.qn1).evaluate(sparse);
		if ((this.qnodes).size() == 1) {
			return sr1;
		}
		sr2 = (this.qn2).evaluate(sparse);
		sr3 = (Logicks.Or(sr1, sr2));
		for (qdex = 2; qdex < (this.qnodes).size(); qdex++) {
			qn3 = (QueryNode) qnodes.get(qdex);
			sr4 = qn3.evaluate(sparse);
			sr3 = (Logicks.Or(sr3, sr4));
		}
		return sr3;
	}

	public void PrintToSystemErr() {
		System.err.println("OrNode:  ");
	}
}

