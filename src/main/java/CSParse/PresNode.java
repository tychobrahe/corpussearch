package CSParse;

import search.Precedes;
import search_result.SentenceResult;
import syntree.SynTree;

class PresNode extends BinarySearchNode {

	public PresNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(Precedes.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(Precedes.Not_y(sparse, x_List, y_List));
		}
		return Record(Precedes.Plain(sparse, x_List, y_List));
	}
}

