package CSParse;

import java.util.Vector;

import search_result.History;
import search_result.SentenceResult;
import syntree.SynTree;

public abstract class QueryNode {
	
	public abstract SentenceResult evaluate(SynTree sparse);

	public abstract void PrintToSystemErr();

	Vector args_for_same;
	Vector curlies_list;
	public History hist = new History();

	public QueryNode() {
	}
	
	public SentenceResult Record(SentenceResult sr) {
		hist.addSentenceResult(sr);
		return (sr);
	}
}

