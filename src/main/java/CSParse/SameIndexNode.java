package CSParse;

import search.SameIndex;
import search_result.SentenceResult;
import syntree.SynTree;

public class SameIndexNode extends BinarySearchNode {

	public SameIndexNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(SameIndex.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(SameIndex.Not_y(sparse, x_List, y_List));
		}
		return Record(SameIndex.Plain(sparse, x_List, y_List));
	}
}

