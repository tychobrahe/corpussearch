package CSParse;

import java.util.Vector;

import basicinfo.ArgList;

public abstract class ThreeListsNode extends QueryNode { // iDomsMod
	ArgNode arg1, arg2, arg3;
	ArgList x_List, y_List, z_List;

	protected ThreeListsNode() {
	}

	protected ThreeListsNode(ArgNode arg1, ArgNode arg2, ArgNode arg3) {
		Init(arg1, arg2, arg3);
	}

	protected void Init(ArgNode arg1, ArgNode arg2, ArgNode arg3) {
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;
		this.x_List = arg1.args_for_search;
		this.y_List = arg3.args_for_search;
		this.z_List = arg2.args_for_search;
		this.args_for_same = new Vector();
		args_for_same.addElement(arg1.prefix_args);
		args_for_same.addElement(arg3.prefix_args);
		this.curlies_list = new Vector();
		curlies_list.addElement(arg1.curlies);
		curlies_list.addElement(arg3.curlies);
		// this.PrintToSystemErr();
	}

	public void PrintToSystemErr() {
		System.err.println("ThreeListsNode:  ");
		System.err.println("x_List: " + this.x_List);
		System.err.println("y_List: " + this.y_List);
		System.err.println("z_List: " + this.z_List);
	}
}

