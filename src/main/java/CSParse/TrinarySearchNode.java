package CSParse;

import java.util.Vector;

import basicinfo.ArgList;

public abstract class TrinarySearchNode extends QueryNode {
	ArgNode arg1, arg2;
	ArgList x_List, y_List;
	IntArgNode intarg1;
	int arg_int;

	protected TrinarySearchNode() {
	}

	protected TrinarySearchNode(ArgNode arg1, IntArgNode intarg1, ArgNode arg2) {
		Init(arg1, intarg1, arg2);
	}

	protected void Init(ArgNode arg1, IntArgNode intarg1, ArgNode arg2) {
		this.arg_int = intarg1.int_arg;
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.x_List = arg1.args_for_search;
		this.y_List = arg2.args_for_search;
		this.args_for_same = new Vector();
		args_for_same.addElement(arg1.prefix_args);
		args_for_same.addElement(arg2.prefix_args);
		this.curlies_list = new Vector();
		curlies_list.addElement(arg1.curlies);
		curlies_list.addElement(arg2.curlies);
		// this.PrintToSystemErr();
	}

	public void PrintToSystemErr() {
		System.err.println("TrinarySearchNode:  ");
		System.err.println("x_List: " + this.x_List);
		System.err.println("y_List: " + this.y_List);
		System.err.println("arg_int: " + this.arg_int);
	}
}

