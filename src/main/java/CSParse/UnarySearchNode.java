package CSParse;

import java.util.Vector;

import basicinfo.ArgList;

public abstract class UnarySearchNode extends QueryNode { // isRoot, Exists
	ArgNode arg1;
	ArgList x_List;

	protected UnarySearchNode() {
	}

	protected UnarySearchNode(ArgNode arg1) {
		Init(arg1);
	}

	protected void Init(ArgNode arg1) {
		this.arg1 = arg1;
		this.x_List = arg1.args_for_search;
		this.args_for_same = new Vector();
		args_for_same.addElement(arg1.prefix_args);
		args_for_same.addElement(arg1.prefix_args);
		this.curlies_list = new Vector();
		curlies_list.addElement(arg1.curlies);
		curlies_list.addElement(arg1.curlies);
		// this.PrintToSystemErr();
	}

	public void PrintToSystemErr() {
		System.err.println("in UnarySearchNode:  ");
		System.err.println("x_List:  " + x_List);
		System.err.println("args_for_same: " + args_for_same);
	}
}

