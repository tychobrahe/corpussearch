package CSParse;

import search.iDomsFirst;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsFirstNode extends BinarySearchNode {

	public iDomsFirstNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(iDomsFirst.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(iDomsFirst.Not_y(sparse, x_List, y_List));
		}
		return Record(iDomsFirst.Plain(sparse, this.x_List, this.y_List));
	}
}

