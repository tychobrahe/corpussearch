package CSParse;

import search.iDomsLast;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsLastNode extends BinarySearchNode {

	public iDomsLastNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(iDomsLast.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(iDomsLast.Not_y(sparse, x_List, y_List));
		}
		return Record(iDomsLast.Plain(sparse, this.x_List, this.y_List));
	}
}

