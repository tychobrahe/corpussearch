package CSParse;

import search.iDomsMod;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsModNode extends ThreeListsNode {

	public iDomsModNode(ArgNode arg1, ArgNode arg2, ArgNode arg3) {
		super.Init(arg1, arg2, arg3);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(iDomsMod.Not_x(sparse, x_List, z_List, y_List));
		}
		if (arg3.IsDenied()) {
			return Record(iDomsMod.Not_y(sparse, x_List, z_List, y_List));
		}
		return Record(iDomsMod.Plain(sparse, x_List, z_List, y_List));
	}
}

