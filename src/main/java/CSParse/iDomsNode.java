package CSParse;

import search.iDominates;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsNode extends BinarySearchNode {

	public iDomsNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(iDominates.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(iDominates.Not_y(sparse, x_List, y_List));
		}
		return Record(iDominates.Plain(sparse, x_List, y_List));
	}
}

