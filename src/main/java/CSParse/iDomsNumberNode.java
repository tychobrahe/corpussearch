package CSParse;

import search.iDomsNumber;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsNumberNode extends TrinarySearchNode {

	public iDomsNumberNode(ArgNode arg1, IntArgNode arg2, ArgNode arg3) {
		super.Init(arg1, arg2, arg3);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg2.IsDenied()) {
			return Record(iDomsNumber.Not_y(sparse, x_List, y_List, arg_int));
		}
		return Record(iDomsNumber.Plain(sparse, x_List, y_List, arg_int));
	}
}

