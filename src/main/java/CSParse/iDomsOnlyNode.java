package CSParse;

import search.iDomsOnly;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsOnlyNode extends BinarySearchNode {

	public iDomsOnlyNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(iDomsOnly.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(iDomsOnly.Not_y(sparse, x_List, y_List));
		}
		return Record(iDomsOnly.Plain(sparse, this.x_List, this.y_List));
	}
}

