package CSParse;

import search.iDomsT;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsTNode extends BinarySearchNode {

	public iDomsTNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(iDomsT.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(iDomsT.Not_y(sparse, x_List, y_List));
		}
		return Record(iDomsT.Plain(sparse, this.x_List, this.y_List));
	}
}

