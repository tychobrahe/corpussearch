package CSParse;

import search.iDomsTotalAssign;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsTotalStuffNode extends HackIntBinarySearchNode {

	public iDomsTotalStuffNode(ArgNode arg1, IntArgNode intarg1, String which) {
		super.Init(arg1, intarg1, which);
	}

	public SentenceResult evaluate(SynTree sparse) {
		return Record(iDomsTotalAssign.Plain(sparse, x_List, arg_int, which_search));
	}
}

