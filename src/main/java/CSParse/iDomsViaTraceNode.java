package CSParse;

import search.iDomsViaTrace;
import search_result.SentenceResult;
import syntree.SynTree;

public class iDomsViaTraceNode extends ThreeListsNode {

	public iDomsViaTraceNode(ArgNode arg1, ArgNode arg2, ArgNode arg3) {
		super.Init(arg1, arg2, arg3);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(iDomsViaTrace.Not_x(sparse, x_List, z_List, y_List));
		}
		if (arg3.IsDenied()) {
			return Record(iDomsViaTrace.Not_y(sparse, x_List, z_List, y_List));
		}
		return Record(iDomsViaTrace.Plain(sparse, x_List, z_List, y_List));
	}
}

