package CSParse;

import search.iPrecedes;
import search_result.SentenceResult;
import syntree.SynTree;

public class iPresNode extends BinarySearchNode {

	public iPresNode(ArgNode arg1, ArgNode arg2) {
		super.Init(arg1, arg2);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(iPrecedes.Not_x(sparse, x_List, y_List));
		}
		if (arg2.IsDenied()) {
			return Record(iPrecedes.Not_y(sparse, x_List, y_List));
		}
		return Record(iPrecedes.Plain(sparse, x_List, y_List));
	}
}

