package CSParse;

import search.inID;
import search_result.SentenceResult;
import syntree.SynTree;

public class inIDNode extends UnarySearchNode {

	public inIDNode(ArgNode arg1) {
		super.Init(arg1);
	}

	public SentenceResult evaluate(SynTree sparse) {
		if (arg1.IsDenied()) {
			return Record(inID.Not_x(sparse, x_List));
		}
		return Record(inID.Plain(sparse, x_List));
	}
}

