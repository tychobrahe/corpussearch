//  Copyright 2010 Beth Randall

/*********************************
This file is part of CorpusSearch.

CorpusSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CorpusSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with CorpusSearch.  If not, see <http://www.gnu.org/licenses/>.
************************************/

package command;

import java.util.Vector;

import basicinfo.ArgList;
import lombok.Getter;
import lombok.Setter;
import revise.TaskList; 

/**
 * stores variables holding information found in query file.
 * 
 * Changed named from CommandInfo to CommandQuery
 * @author Luiz Veronesi on initial fork
 */
@Getter @Setter
public class CommandQuery {
    
	public boolean use_prefs = false;
    public String prefs_name;

    // start off variables with their default values.
    // these variables are given their final values by ReadIn.java.
    public boolean multi_output = false;
    public boolean do_frames = false;
    public boolean make_lexicon = false;
    public boolean make_label_lexicon = false;

    public boolean has_command_file = true;
    public String char_encoding = "ASCII";
    public boolean copy_corpus = false;
    public boolean reformat_corpus = false;
    public boolean print_only = false;
    public boolean print_complement = false;
    public String output_format = "QINGTIAN";
    public String recon_str = "";
    public boolean reconstruct = false;
    public ArgList recon_arg;
    public String lonely = "QINGTIAN";
    public ArgList lonely_list;
    public boolean use_def_file = false;
    public boolean revise = false;
    public Vector curlies;
    public TaskList tasker = new TaskList();

    public String append_to_CODING = "";
    public String node = "NO_NODE_BOUNDARY_FOUND";
    public String orig_coding_query = "NO_ORIG_CODING_QUERY_FOUND";
    public String coding_query = "NO_CODING_QUERY_FOUND";
    public boolean do_coding = false;
    public String query = "NO_QUERY_FOUND";
    //public String ignore = "CODE|LB|'|\"|,|E_S|/|RMV*|" + "\\" + "**";
    public String ignore = "CODE|LB|'|E_S|COMMENT*|RMV*|" + "\\" + ".|" + ",|" + "\\" + "\"";
    public String word_ignore = "CODE|0|LB|'|E_S|COMMENT*|RMV*|" + "\\" + "**|" + "\\" + ".|" + "\\" + "\"|" + "," ;
    public String orig_query = "";
    public String def_name = "";

    // for CorpusDraw, see drawtree package.
    public boolean show_only = false;
    public String show_str = "";
    public boolean make_tag_list = false;

    public boolean gotQuery() {
	if (query.equals("NO_QUERY_FOUND")) {
	    return false; }
	return true; }
} 