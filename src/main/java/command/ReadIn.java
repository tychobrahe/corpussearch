//  Copyright 2010 Beth Randall

/*********************************
This file is part of CorpusSearch.

CorpusSearch is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CorpusSearch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with CorpusSearch.  If not, see <http://www.gnu.org/licenses/>.
************************************/

package command;

import java.io.*;
import java.util.*;
import io.*;
import basicinfo.*;
import print.*;

public class ReadIn extends CommandQuery {

	public static void PrefsAndQuery(CommandQuery commandQuery) {
		//IoInfo.newComments();
		AfterCommands(commandQuery);
	}

	public static void PrefsAndQuery(String commands_name, CommandQuery commandQuery) {
		//IoInfo.newComments();
		if (commandQuery.use_prefs) {
			// Parameters.has_prefs = true;
			// Parameters.prefs_name = prefs_name;
			CommandFile(commandQuery.prefs_name, commandQuery);
		}
		if (commandQuery.has_command_file) {
			CommandFile(commands_name, commandQuery);
		}
		AfterCommands(commands_name, commandQuery);
	}

	@Deprecated
	public static void CommandFile(String commands_name, CommandQuery commandQuery) {
		String commando = "";
		boolean found_command = false;
		InFileDominatrix infile;
		BasicCommands basicCommands = new BasicCommands();
		try {
			infile = new InFileDominatrix(commands_name);
			read_loop: while (!infile.EOF) {
				commando = infile.NextString();
				if (infile.EOF) {
					break read_loop;
				}
				found_command = basicCommands.Store(commando, infile, commandQuery);
				if (!found_command) {
					BasicCommands.CommandNotFoundError(commando, infile);
				}
			}
			infile.Close();
		} catch (Exception e) {
			System.err.println("In ReadIn.PrefsFile:  ");
			System.err.println(e.getMessage());
			e.printStackTrace();
		} finally {
			return;
		}
	}

	@Deprecated
	public static void AfterCommands(CommandQuery commandQuery) {
		Vitals.Init(commandQuery.node, commandQuery.ignore, commandQuery.word_ignore);
		Parameters.Init();
		return;
	}

	@Deprecated
	public static void AfterCommands(String commands_name, CommandQuery commandQuery) {

		CommandAux.CheckNodeBoundary(commandQuery);
		Vitals.Init(commandQuery.node, commandQuery.ignore, commandQuery.word_ignore);
		Parameters.Init();
		if (commandQuery.print_only) {
			commandQuery.lonely_list = new ArgList(commandQuery.lonely);
		}
		if (commandQuery.use_def_file) {
			if (commandQuery.do_coding) {
				commandQuery.coding_query = Definitions.InstallDefs(commandQuery.orig_coding_query);
			} else {
				commandQuery.query = Definitions.InstallDefs(commandQuery.orig_query);
			}
		}
		return;
	}

}
