//  Copyright 2010 Beth Randall

/*********************************
 This file is part of CorpusSearch.

 CorpusSearch is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 CorpusSearch is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with CorpusSearch.  If not, see <http://www.gnu.org/licenses/>.
 ************************************/

package csearch;

import io.IoInfo;
import io.OutFileDominatrix;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringReader;

import print.PrintOut;
import stats.StatsPerFile;
import stats.StatsPerSearch;
import CSParse.ParseQuery;
import basicinfo.Goodbye;

import command.CommandQuery;
import command.ReadIn;

public class Meat extends CorpusSearch {

	public static String file_name = "UNKNOWN_FILE";
	public static boolean searching_output = false;
	public static StatsPerFile file_info, comp_file_info;
	public static StatsPerSearch search_info, comp_search_info;
	public static boolean empty_source = false;
	public static File source_file;
	public static OutFileDominatrix out_dom, comp_dom;
	public static PrintWriter OutStuff, CompStuff;

	/*
	 * CrankThrough -- cranks through input file.
	 */
	public void CrankThrough(String query, CommandQuery commandQuery) {
		int i;
		try {
			ReadIn.PrefsAndQuery(command_name, commandQuery);
			if (commandQuery.print_complement) {
				comp_name = dest_name.substring(0, dest_name.lastIndexOf("."));
				comp_name += ".cmp";
				comp_dom = new OutFileDominatrix(comp_name);
				CompStuff = comp_dom.getPrintWriter();
			}
			if (commandQuery.print_only) {
				MeatOnly.CrankThrough(commandQuery);
				return;
			}
			if (commandQuery.reformat_corpus) {
				MeatReformat.CrankThrough();
				return;
			}
			if (commandQuery.copy_corpus) {
				MeatCopy.CrankThrough(commandQuery);
				return;
			}
			search_info = new StatsPerSearch(command_name, dest_name);
			if (commandQuery.print_complement) {
				comp_search_info = new StatsPerSearch(command_name, comp_name);
			}
			// ReadAux.CheckNodeBoundary();
			if (command_name.endsWith(".c") || commandQuery.coding_query != "NO_CODING_QUERY_FOUND") {
				dest_name = dest_name.substring(0, dest_name.lastIndexOf("."));
				dest_name += ".cod";
				super.MakeDestinationFile(commandQuery);
				out_dom = new OutFileDominatrix(dest_name);
				OutStuff = out_dom.getPrintWriter();
				MeatCoding.CrankThrough(commandQuery);
				return;
			}
			super.MakeDestinationFile(commandQuery);
			// if (!CommandInfo.multi_output) {
			if (commandQuery.make_tag_list) {
				dest_name = dest_name.substring(0, dest_name.lastIndexOf("."));
				dest_name += ".tag";
				out_dom = new OutFileDominatrix(dest_name);
				OutStuff = out_dom.getPrintWriter();
				PrintOut.PrefaceTagList(dest_name, command_name, OutStuff, source_list, commandQuery);
				MeatTags.CrankThrough();
				return;
			}
			out_dom = new OutFileDominatrix(dest_name, commandQuery.output_format);
			OutStuff = out_dom.getPrintWriter();
			// out_dom.report();
			/*
			if (IoInfo.getInputFormat().equals("ottawa")) {
				MeatOtt.CrankThrough(commandQuery);
				return;
			}
			*/
			if (commandQuery.do_frames) {
				PrintOut.PrefaceFrames(dest_name, command_name, OutStuff, source_list, commandQuery);
				MeatFrames.CrankThrough(commandQuery);
				return;
			}
			if (commandQuery.make_lexicon) {
				PrintOut.PrefaceLexicon(dest_name, command_name, OutStuff, source_list, commandQuery);
				MeatLexicon.CrankThrough();
				return;
			}
			if (commandQuery.make_label_lexicon) {
				PrintOut.PrefaceLabelLexicon(dest_name, command_name, OutStuff, source_list, commandQuery);
				MeatLabelLexicon.CrankThrough();
				return;
			}
			// if (!CommandInfo.multi_output) {
			// PrintOut.Preface(dest_name, command_name,
			// OutStuff, source_list); }
			if (commandQuery.print_complement) {
				PrintOut.PrefaceComplement(dest_name, command_name, CompStuff, source_list, commandQuery);
			}
			System.err.println("Working.  Please be patient.");
			// work through list of source files.
			arg_loop: for (i = 0; i < source_list.size(); i++) {
				file_name = (String) source_list.elementAt(i);
				if (file_name.equals("-out")) {
					break arg_loop;
				}
				if (file_name.endsWith(".out")) {
					searching_output = true;
				}
				source_file = new File(file_name);
				MeatAux.CheckSource();
				if (i == 0) {
					super.seekPrefs(file_name, commandQuery);
					PrintOut.Preface(dest_name, command_name, OutStuff, source_list, commandQuery);
					//ParseQuery.makeQueryTree(new StringReader(query));
					//parseQuery.makeQueryTree(commandQuery);
					ParseQuery parseQuery = new ParseQuery(commandQuery);
					ReviseStuff(commandQuery);
				}

				empty_source = true;

				SentenceLoop.thruFile(file_name, commandQuery);

			} // end arg_loop
			PrintOut.BigFooter(search_info, OutStuff);
			OutStuff.flush();
			if (commandQuery.print_complement) {
				PrintOut.BigFooter(comp_search_info, CompStuff);
				CompStuff.flush();
			}
			OutMessage(dest_name, commandQuery);
		} // end try
		catch (Exception e) {
			System.err.println("ERROR!  In Meat.CrankThrough:  ");
			System.err.println("Exception:  " + e.getMessage());
			System.err.println(e.getMessage());
			System.err.println(e.toString());
			e.printStackTrace();
			Goodbye.SearchExit();
		} finally {
			return;
		}
	} // end method CrankThrough

	protected static void ReviseStuff(CommandQuery commandQuery) {
		if (commandQuery.revise) {
			(commandQuery.tasker).SetCurlers(commandQuery.curlies);
		}
		// (CommandInfo.tasker).PrintToSystemErr(); }
		return;
	}

	private void OutMessage(String dest_name, CommandQuery commandQuery) {
		System.err.println("Output file is ");
		System.err.println("    " + dest_name);
		if (commandQuery.print_complement) {
			System.err.println("Complement file is ");
			System.err.println("    " + comp_name);
		}
		return;
	}
}
